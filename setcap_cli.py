#!/usr/bin/env python3

import socket
import argparse
import json
import getpass
import os

def connectToServer(host='localhost', port=4242):
    s = None
    for res in socket.getaddrinfo(host, port, socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except OSError as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except OSError as msg:
            s.close()
            s = None
            continue
        break

    if s is None:
        print('could not open socket')

    return s

def sendData(s, data):
    msg = json.dumps(data)
    s.sendall(msg.encode('utf-8'))

def main():
    parser = argparse.ArgumentParser(description='Permission client.')
    parser.add_argument('host', metavar='host', nargs='?',
                        default='localhost', help='remote host name')
    parser.add_argument('-p', '--port', metavar='port', type=int, nargs='?',
                        default=4242, help='port on remote server')
    parser.add_argument('-u', '--user', metavar='user', nargs='?',
                        default=getpass.getuser(),
                        help='user to send data as')
    parser.add_argument('path', metavar='path',
                        default=os.getcwd(),
                        help='relative path to executable')    

    args = parser.parse_args()

    if not os.path.exists(args.path):
        print('path does not exist')
        return   

    if not os.path.isfile(args.path):
        print('not a file!')
        return

    data = {
        'user': args.user,
        'path':os.path.abspath(args.path)
    }

    s = connectToServer(args.host, args.port)
    if s is not None:
        sendData(s, data)
        s.close()

if __name__ == '__main__':
    main()
