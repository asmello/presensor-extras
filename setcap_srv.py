#!/usr/bin/env python3

import os
import json
import socket
import signal
import threading
import argparse
import subprocess

sockets = []

def handle_SIGINT(sig, frame):
    for s in sockets:
        s.shutdown(socket.SHUT_RDWR)

def createServer(port=4242, users=[]):
    s = None
    for res in socket.getaddrinfo(None, port, socket.AF_UNSPEC,
                                  socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except OSError as msg:
            s = None
            continue
        try:
            s.bind(sa)
            s.listen(1)
        except OSError as msg:
            s.close()
            s = None
            continue
        break

    if s is None:
        print('could not open socket')
        return

    sockets.append(s)

    while True:
        try:
            conn, addr = s.accept()
            sockets.append(conn)
            t = threading.Thread(target=handle_connection,
                                args=(conn, addr, users))
            t.start()
        except InterruptedError:
            continue
        except OSError as msg:
            break

    s.close()
        
def handle_connection(s, addr, users):
    while True:
        try:
            bytes = s.recv(4096)
            if not bytes: break
            msg = bytes.decode('utf-8')
            data = json.loads(msg)

            if data['user'] in users:
                path = os.path.abspath(data['path'])
                if not path.startswith('/home/' + data['user']):
                    continue
                print('enabling capabilities for ' + path)
                subprocess.call(['setcap', "cap_net_raw,cap_net_admin+eip", 
                                path])
        except InterruptedError:
            continue
    sockets.remove(s)
    s.close()

def main():

    if os.geteuid() != 0:
        print('need root priviledges... try sudo?')
        return

    parser = argparse.ArgumentParser(description='Permission server.')
    parser.add_argument('-p', '--port', metavar='port', type=int, nargs='?',
                        default=4242, help='port to listen on')
    parser.add_argument('users', metavar='user', nargs='+',
                        help='whitelisted users')
    args = parser.parse_args()

    signal.signal(signal.SIGINT, handle_SIGINT)
    createServer(args.port, args.users)

if __name__ == "__main__":
    main()
