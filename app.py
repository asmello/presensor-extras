#!flask/bin/python3
from functools import wraps
from flask import Flask, jsonify, request, Response
import pprint

app = Flask(__name__)

users = {
    'sensor1' : 'passwd'
}

def get_password(username):
    if username in users:
        return users.get(username)
    return None

def check_auth(username, password):
    """Checks if username/password combination is valid"""
    return get_password(username) == password;

def unauthorized_response(username=None, password=None):
    rstr = 'This URL requires authentication.\n' \
           'You must login with the proper credentials.\n'
    if username: rstr = rstr + 'user: ' + username + '\n'
    if password: rstr = rstr + 'pass: ' + password + '\n'
    return Response(rstr, 401,
                    {'WWW-Authenticate' : 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        cred = request.authorization
        if not cred or not check_auth(cred.username, cred.password):
            if cred: return unauthorized_response(cred.username, cred.password)
            return unauthorized_response()
        return f(*args, **kwargs)
    return decorated

class APIException(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(APIException)
def handle_invalid_credentials(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/')
@requires_auth
def index():
    return 'Hello, world!\n'

@app.route('/api/auth', methods=['GET'])
def auth():
    try:
        pin = request.args['pin']
    except:
        raise APIException('PIN was not specified')
    if pin != '1234':
        raise APIException('Invalid PIN.', status_code=401)
    auth = { 'user' : 'sensor1', 'pass' : 'passwd' }
    return jsonify(**auth)

@app.route('/api/alert', methods=['POST'])
def event():
    data = request.get_json()
    if not data:
        raise APIException('Invalid JSON syntax')
    try:
        print('[i] Incoming JSON payload...')
        print('[i] User-Agent: ' + request.headers.get('User-Agent'))
        if not check_auth(data['user'], data['pass']):
            print('[x] Invalid credentials. Aborting.')
            return 'Invalid credentials.'
        print('[i] Sensor: ' + data['user'])
        for alert in data['alerts']:
            print('[i] Timestamp: ' + alert['time'])
            if alert['kind'] == 'in':
                print('[i] IN alert:')
                print('> DIST: ' + str(alert['distance']))
            elif alert['kind'] == 'out':
                print('[i] OUT alert:')
            elif alert['kind'] == 'update':
                print('[i] UPDATE alert:')
                print('> DIST: ' + str(alert['distance']))
            else:
                print('[!] unknown alert:')
            print('> BeaconID: ' + alert['uuid'] + ','
                  + str(alert['major']) + ',' + str(alert['minor']))
    except Exception as e:
        print('[x] Exception: ' + str(e))
        raise APIException('Invalid JSON payload')
    return ""

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
